
# Detects which OS and if it is Linux then it will detect which Linux Distribution.

system_informations()
{
	OS=`uname -s`
	REV=`uname -r`
	MACH=`uname -m`


	if [ "${OS}" = "Linux" ] ; then
		KERNEL=`uname -r`
		if [ -f /etc/redhat-release ] ; then
			DIST='RedHat'
			PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
			REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
		fi
	
		OSSTR="${OS} ${DIST} ${REV}(${PSUEDONAME} ${KERNEL} ${MACH})"
	else
		cecho "${i18n[5001]}\\n" "red"
		exit 0
	fi
}