set_language() {
	cecho "Select a language you want:\\n"
	printf "    1  - English (en-US)\n"
	printf "    2  - Português Brasil (pt-BR)\n"
	printf "Option: "
	read lang
	case $lang in
		1) eval "$(curl -fsSL https://bitbucket.org/welington_sampaio/rpainel_install/raw/master/languages/en-us)" ;;
		2) eval "$(curl -fsSL https://bitbucket.org/welington_sampaio/rpainel_install/raw/master/languages/pt-br)" ;;
		*)
		cecho "\\tOption selected is invalid\\n" "red"
		sleep 2
		set_language
		;;
	esac
}