# message
# color opts green, red, blue, yellow
cecho() {
	blue='\E[0;34m'
	green='\E[0;32m'
	red='\E[0;31m'
	yellow='\E[33;47m'
	default_color='green'
	color=${2:-$default_color}
	if [ $color = 'blue' ]; then color=$blue; fi;
	if [ $color = 'green' ]; then color=$green; fi;
	if [ $color = 'red' ]; then color=$red; fi;
	if [ $color = 'yellow' ]; then color=$yellow; fi;
	printf "$color$1"
	reset
}